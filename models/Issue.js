const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const closedSprintsSchema = new Schema({
  id: {
    type: Number
  },
  self: {
    type: String
  },
  state: {
    type: String
  },
  name: {
    type: String
  },
  startDate: {
    type: Date
  },
  endDate: {
    type: Date
  },
  completeDate: {
    type: Date
  },
  goal: {
    type: String
  }
});
const milestoneSchema = new Schema({
  project_id: {
    type: Number
  },
  description: {
    type: String
  },
  state: {
    type: String
  },
  due_date: {
    type: Date
  },
  iid: {
    type: Number
  },
  created_at: {
    type: Date
  },
  title: {
    type: String
  },
  id: {
    type: Number
  },
  updated_at: {
    type: Date
  }
});
const assigneesSchema = new Schema({
  state: {
    type: String
  },
  id: {
    type: Number
  },
  name: {
    type: String
  },
  web_url: {
    type: String
  },
  avatar_url: {
    type: String
  },
  username: {
    type: String
  }
});

const timestatsSchema = new Schema({
  time_estimate: {
    type: Date
  },
  total_time_spent: {
    type: Date
  },
  human_time_estimate: {
    type: Date
  },
  human_total_time_spent: {
    type: Date
  }
});
const fieldsSchema = new Schema({
  flagged: {
    type: Boolean
  },
  sprint: {
    type : Schema.Types.ObjectId,
    ref : "sprints"
  }
});
// Create Schema
const IssueSchema = new Schema({
  //Donnees API Jira :Get Sprint issues for a user
  avatar_url: {
    type: String
  },
  fields: {
    fieldsSchema
  },
  closedSprints: [
    {
      closedSprintsSchema
    }
  ],

  description: {
    type: String
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: projects
  },
 
 
  //Donnees API Gitlab :Get all issues the authenticated user has access to.

  state: {
    type: String
  },
  description: {
    type: String
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: users
  },
  milestone: {
    milestoneSchema
  },
  project_id: {
    type: Number
  },
  assignees: [
    {
      assigneesSchema
    }
  ],
  assignee: {
    assigneesSchema
  },
  updated_at: {
    type: Date
  },
  closed_at: {
    type: String
  },
  closed_by: {
    type: String
  },
  id: {
    type: Number
  },
  title: {
    type: String
  },
  created_at: {
    type: Date
  },
  iid: {
    type: Number
  },
  labels: [],
  upvotes: {
    type: Number
  },
  downvotes: {
    type: Number
  },
  merge_requests_count: {
    type: Number
  },
  user_notes_count: {
    type: Number
  },
  due_date: {
    type: Date
  },
  web_url: {
    type: String
  },
  confidential: {
    type: Boolean
  },
  weight: {
    type: Number
  },
  discussion_locked: {
    type: Boolean
  },
  time_stats: {
    timestatsSchema
  }
});

module.exports = Issue = mongoose.model("issues", IssueSchema);
