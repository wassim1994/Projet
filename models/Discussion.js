const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
//You can leave a comment in the following places:issues/epics/merge/requests/snippets/commits/commit diffs
//Gitlab APi
const notesSchema = new Schema({
  id: {
    type: Number
  },
  type: {
    type: String
  },
  body: {
    type: String
  },
  attachment: {
    type: String
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  created_at: {
    type: Date
  },
  updated_at: {
    type: Date
  },
  system: {
    type: String
  },
  noteable_id: {
    type: Number
  },
  noteable_type: {
    type: String
  },
  noteable_iid: {
    type: Number
  }
});
const DiscussionSchema = new Schema({
  Discussions: [
    {
      id: {
        type: String
      },
      individual_note: {
        type: Boolean
      },
      notes: [notesSchema]
    }
  ],
  issue: {
    type: Schema.Types.ObjectId,
    ref: "issues"
  }
});
module.exports = Discussion = mongoose.model("discussions", DiscussionSchema);
