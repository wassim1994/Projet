const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const assigneeSchema = new Schema({
  originalEstimate: {
    type: String
  },
  remainingEstimate: {
    type: Number
  },
  timeSpent: {
    type: Number
  },
  originalEstimateSeconds: {
    type: Number
  },
  remainingEstimateSeconds: {
    type: Number
  },
  timeSpentSeconds: {
    type: Number
  }
});

const milestoneSchema = new Schema({id: {
  type: Number
},
iid: {
  type: Number
},
project_id: {
  type: Number
},
title: {
  type: String
},
description: {
  type: String
},
state: {
  type: String
},
created_at: {
  type: Date
},
updated_at: {
  type: Date
},
due_date: {
  type: Date
},
start_date: {
  type: Date
},
web_url: {
  type: String
}});

const timeStatsSchema = new Schema ({time_estimate: {
  type: Boolean
},
total_time_spent: {
  type: Boolean
},
human_time_estimate: {
  type: Boolean
},
human_total_time_spent: {
  type: Boolean
}});
// Create Schema
const MergeSchema = new Schema({
  //Donnees API Gitlab :Get all merge requests the authenticated user has access to

  id: {
    type: Number
  },
  iid: {
    type: Number
  },
  project_id: {
    type: Number
  },
  title: {
    type: String
  },
  description: {
    type: String
  },
  state: {
    type: String
  },
  merged_by: {
    // ref to user
  },
  merged_at: {
    type: Date
  },
  closed_by: {
    type: String
  },
  closed_at: {
    type: String
  },
  created_at: {
    type: Date
  },
  updated_at: {
    type: Date
  },
  target_branch: {
    type: String
  },
  source_branch: {
    type: String
  },
  upvotes: {
    type: Number
  },
  downvotes: {
    type: Number
  },
  author: {
    // ref to user
  },
  assignee: {
    assigneeSchema
  },
  source_project_id: {
    type: Number
  },
  target_project_id: {
    type: Number
  },
  labels: [],
  work_in_progress: {
    type: Boolean
  },
  milestone: {
    milestoneSchema
  },
  merge_when_pipeline_succeeds: {
    type: Boolean
  },
  merge_status: {
    type: String
  },
  sha: {
    type: Number
  },
  merge_commit_sha: {
    type: String
  },
  user_notes_count: {
    type: Number
  },
  discussion_locked: {
    type: String
  },
  should_remove_source_branch: {
    type: Boolean
  },
  force_remove_source_branch: {
    type: Boolean
  },
  allow_collaboration: {
    type: Boolean
  },
  allow_maintainer_to_push: {
    type: Boolean
  },
  web_url: {
    type: String
  },
  time_stats: {
    timeStatsSchema
  },
  squash: {
    type: Boolean
  },
  approvals_before_merge: {
    type: String
  },

  
//Refs
      user: {
        type: Schema.Types.ObjectId,
        ref: "users"
      },
   author: {
        type: Schema.Types.ObjectId,
        ref: "users"
      },
      merged_by: {
        type: Schema.Types.ObjectId,
        ref: "users"
      },
});

module.exports = Merge = mongoose.model("merges", MergeSchema);
