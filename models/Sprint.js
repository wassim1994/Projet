const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const SprintSchema = new Schema(
  {

    id : {
        type : Number
      },
      self : {
        type: String
      },
      state: {
        type: String
      },
      name: {
        type: String
      },
      startDate: {
        type : Date
      },
      endDate: {
        type :Date
      },
      completeDate : {
        type : Date
      },
      origingBoardId: {
        type : Number
      },
      goal : {
        type : String
      }
  });

  module.exports = Sprint = mongoose.model("sprints", SprintSchema);
