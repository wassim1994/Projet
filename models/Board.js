const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const BoardSchema = new Schema({
  id: {
    type: String
  },
  self: {
    type: String
  },
  name: {
    type: String
  },
  type: {
    type: String
  },
  // Reference to Dashboard
  dashboard: {
    type: Schema.Types.ObjectId,
    ref: "dashboards"
  }
});

module.exports = Board = mongoose.model("boards", BoardSchema);
