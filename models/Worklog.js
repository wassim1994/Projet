const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema

const visibilitySchema = new Schema ({ type:{
    type:String
},
value:{
    type:String
}});
//Get worklog time estimation for an issue :Time tracking
const WorklogSchema = new Schema(
  {
        self: {
            type:String
        },
       
        autor: {
            type: Schema.Types.ObjectId,
            ref: "users",
        }, 
        updateAuthoruser: {
            type: Schema.Types.ObjectId,
            ref: "users",
        }, 
        epic: {
            type: Schema.Types.ObjectId,
            ref: "epics",
        }, 
        updated: {
            type:Date
        },
        visibility: 
        {
            visibilitySchema
     }, 
     started: {
        type:Date
    },
    timeSpent:{
        type:String
    }, 
    timeSpentSeconds: {
        type:Number
    },
    id:{
        type:Number
    },
    issueId :{
        type:Number
    }
  });

  module.exports = Worklog = mongoose.model("worklogs",WorklogSchema);
