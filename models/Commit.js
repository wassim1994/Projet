const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema

const statsSchema = new Schema({
  additions: {
    type: Number
  },
  deletions: {
    type: Number
  },
  total: {
    type: Number
  }
});
const lastpipelineSchema = new Schema({
  id: {
    type: Number
  },
  ref: {
    type: String
  },
  sha: {
    type: String
  },
  status: {
    type: String
  }
});

const CommitSchema = new Schema({
  id: {
    type: String
  },
  short_id: {
    type: String
  },
  title: {
    type: String
  },
  author_name: {
    type: String
  },
  author_email: {
    type: String
  },
  committer_name: {
    type: String
  },
  committer_email: {
    type: String
  },
  created_at: {
    type: Date
  },
  message: {
    type: String
  },
  committed_date: {
    type: Date
  },
  authored_date: {
    type: Date
  },
  parents_ids: [
    {
      id: {
        type: String
      }
    }
<<<<<<< HEAD
  ],
  last_pipeline: {
    lastpipelineSchema
  },
  stats: {
    statsSchema
  },
  status: {
    type: String
  },
  //Ref
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  Branch : {
      type : Schema.Types.ObjectId,
      ref : "branches"
  }
});
=======
},
status : {
    type : String 
},
//Ref
user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },

    });

>>>>>>> cefbbd6f70685e8b08b7f6efd9e280633fc76ec0
module.exports = Commit = mongoose.model("commits", CommitSchema);
