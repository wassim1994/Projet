const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const WikisSchema = new Schema({

//Get all wiki pages for a given project.
//Gitlab API
Wikis:[
    {
        content : {
            type:String
        },
        format : {
            type:String
        },
        slug : {
            type:String
        },
        title : {
            type:String
        },
      },
     
],

Projet: {
    type: Schema.Types.ObjectId,
    ref: "projets"
  }
});
  module.exports = Wiki = mongoose.model("wikis", WikisSchema);
  



