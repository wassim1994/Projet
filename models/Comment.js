const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// GET /projects/:id/repository/commits/:sha/comments
const visibilitySchema = new Schema({
  type: {
    type: String
  },
  value: {
    type: String
  }
});
const bodySchema = new Schema({
  type: {
    type: String
  },
  version: {
    type: Number
  },
  content: [
    {
      type: {
        type: String
      },
      content: [
        {
          type: {
            type: String
          },
          text: {
            type: String
          }
        }
      ]
    }
  ]
});
// Create Schema
const CommentSchema = new Schema({
  self: {
    type: String
  },
  id: {
    type: Number
  },
  note: {
    type: String
  },
  author: {
    // ref to user
  },
  body: {
    bodySchema
  },
  updateAuthor: {
    //user
  },
  created: {
    type: Date
  },
  updated: {
    type: Date
  },
  visibility: {
    visibilitySchema
  },
  //Ref
  Issue: {
    type: Schema.Types.ObjectId,
    ref: "issues"
  },
  Commit: {
    type: Schema.Types.ObjectId,
    ref: "commits"
  },
  Worklog: {
    type: Schema.Types.ObjectId,
    ref: "worklogs"
  },
  Users: {
    type: Schema.Types.ObjectId,
    ref: "users"
  }
});

module.exports = Comment = mongoose.model("comments", CommentSchema);
