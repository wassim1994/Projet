const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema

const itemsSchema = new Schema({
    field: {
      type: String
    },
    fieldtype: {
      type: String
    },
    fieldId: {
      type: String
    },
    from: {
      type: String
    },
    fromString: {
      type: String
    },
    to: {
      type: String
    },
    toString: {
      type: String
    }});

    const valuesSchema = new Schema(  {
        id: {
          type: Number
        },
        author: {
          type : Schema.Types.ObjectId,
          ref: "users"
        },
        created: {
          type: Date
        },
        items: [
          {
              itemsSchema
          }
        ]
      },
      {
        items: [
          {
            itemsSchema
          }
        ]
      });
//Get changelog issue:Returns all changelogs for an issue sorted by date, starting from the oldest.
const ChangelogSchema = new Schema({
  self: {
    type: String
  },
  nextPage: {
    type: String
  },
  maxResults: {
    type: Number
  },
  startAt: {
    type: Number
  },
  total: {
    type: Number
  },
  isLast: {
    type: Boolean
  },
  values: [
   valuesSchema
  ],
  issue: {
    type: Schema.Types.ObjectId,
    ref: "issues"
  }
});

module.exports = Changelog = mongoose.model("changelogs", ChangelogSchema);
