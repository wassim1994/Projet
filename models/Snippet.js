const mongoose = require("mongoose");
const Schema = mongoose.Schema;



const SnippetSchema = new Schema({
//With GitLab Snippets you can store and share bits of code and text with other users.
//Get all snippets for a user
//Gitlab API
Snippets:[
    {
        id: {
            Type:Number
        },
        title:{
            Type:String
        }, 
        file_name:{
            Type:String
        }, 
        description:{
            Type:String
        }, 
        visibility:{
            Type:String
        }, 
        author: {
          type : Schema.Types.ObjectId,
          ref : "users"
        },
        updated_at:{
            Type:Date
        }, 
        created_at:{
            Type:Date
        }, 
        project_id:{
            Type:Number
        }, 
        web_url:{
            Type:String
        }, 
        raw_url:{
            Type:String
        }, 
    },
    
],

issue: {
    type: Schema.Types.ObjectId,
    ref: "issues"
  }
});
  module.exports = Snippet = mongoose.model("snippets", SnippetSchema);
  



