const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const tagListSchema = new Schema({
  label: {
    type: String
  }
});
const nameSpaceSchema = new Schema({
  id: {
    type: String
  },
  name: {
    type: String
  },
  path: {
    type: String
  },
  kind: {
    type: String
  },
  full_path: {
    type: String
  }
});
const ownerSchema = new Schema({
  id: {
    type: String,
    required: true
  },
  name: {
    type: String
  },
  created_at: {
    type: Date
  }
});
const permissionsSchema = new Schema({
  project_access: {
    access_level: {
      type: Number
    },
    notification_level: {
      type: Number
    }
  },
  group_access: {
    access_level: {
      type: Number
    },
    notification_level: {
      type: Number
    }
  }
});
const licenseSchema = new Schema({
  key: {
    type: String
  },
  name: {
    type: String
  },
  nickname: {
    type: String
  },
  html_url: {
    type: String
  },
  source_url: {
    type: String
  }
});

const shared_with_groupsSchema = new Schema({
  groupe_id: { type: Number },
  groupe_name: { type: String },
  group_full_path: { type: String },
  group_access_level: { type: Number }
});
const statisticsSchema = new Schema({
  commit_count: {
    type: Number
  },
  storage_size: {
    type: Number
  },
  repository_size: {
    type: Number
  },
  lfs_objects_size: {
    type: Number
  },
  job_artifacts_size: {
    type: Number
  }
});
const linksSchema = new Schema({
  self: {
    type: String
  },
  issues: {
    type: String
  },
  merge_requests: {
    type: String
  },
  repo_branches: {
    type: String
  },
  labels: {
    type: String
  },
  events: {
    type: String
  },
  members: {
    type: String
  }
});
const avatarurlsSchema = new Schema({
  avatar_url_gitlab: {
    type: String
  },
  av_48: {
    type: String
  },
  av_24: {
    type: String
  },
  av_16: {
    type: String
  },
  av_32: {
    type: String
  }
});

const projectCategorySchema = new Schema({
  self: {
    type: String
  },
  id: {
    type: String
  },
  name: {
    type: String
  },
  description: {
    type: String
  }
});
// Create Project Schema
const ProjetSchema = new Schema({
  // Data from API GitLab : Get Single project

  // Project Id Gitlab
  id_gitlab: {
    type: String,
    required: true
  },
  // Short project description
  description: {
    type: String
  },
  // Visibility : Private , Public, internal
  visibility: {
    type: String
  },
  // Default branch Master
  default_branch: {
    type: String,
    required: true
  },
  // SSH Url To Repostory
  ssh_url_to_repo: {
    type: String
  },
  // Http Url to Repostory
  http_url_to_repo: {
    type: Date
  },
  // Web Url
  web_url: {
    type: String
  },
  // Url of Readme File
  readme_url: {
    type: String
  },
  // Tags List sub doc
  tag_list: [tagListSchema],
  // Owner of Project
  owner: { 
    type: Schema.Types.ObjectId, 
    ref: "users" 
  },
  // Name of project
  name: {
    type: String,
    required: true
  },
  name_with_namespace: {
    type: String
  },
  path: {
    type: String
  },
  path_with_namespace: {
    type: String
  },
  //  Enable issues for this project
  issues_enabled: {
    type: Boolean
  },
  open_issues_count: {
    type: Number
  },
  // Enable MR for this project
  merge_requests_enabled: {
    type: Boolean
  },
  // Enable Jobs for this project
  jobs_enabled: {
    type: Boolean
  },
  // Enable wiki for this project
  wiki_enabled: {
    type: Boolean
  },
  // Enable snippets for this project
  snippets_enabled: {
    type: Boolean
  },
  resolve_outdated_diff_discussions: {
    type: Boolean
  },
  container_registry_enabled: {
    type: Boolean
  },
  created_at: {
    type: Date
  },
  last_activity_at: {
    type: Date
  },
  creator_id: {
    type: Date
  },
  namespace: { nameSpaceSchema },
  import_status: {
    type: String
  },
  import_error: {
    type: Boolean
  },
  archived: {
    type: Boolean
  },
  permissions: { permissionsSchema },

  license_url: {
    type: String
  },
  license: { licenseSchema },
  shared_runners_enabled: {
    type: Boolean
  },
  forks_count: {
    type: Number
  },
  star_count: {
    type: Number
  },
  runners_token: {
    type: String
  },
  public_jobs: {
    type: Boolean
  },
  shared_with_groups: [shared_with_groupsSchema],
  repository_storage: {
    type: String
  },
  only_allow_merge_if_pipeline_succeeds: {
    type: Boolean
  },
  only_allow_merge_if_all_discussions_are_resolved: {
    type: Boolean
  },
  printing_merge_requests_link_enabled: {
    type: Boolean
  },
<<<<<<< HEAD

=======
>>>>>>> cefbbd6f70685e8b08b7f6efd9e280633fc76ec0
  request_access_enabled: {
    type: Boolean
  },
  merge_method: {
    type: String
  },
  approvals_before_merge: {
    type: String
  },
  statistics: { statisticsSchema },
  _links: { linksSchema },
  // Data from API Jira : GET Project
  self: {
    type: String
  },
  id_jira: {
    type: Number
  },
  key_jira: {
    type: String
  },
  name_jira: {
    type: String
  },
  avatar_urls: {
    avatarurlsSchema
  },
  projectCategory: {
    projectCategorySchema
  },
  simplified: {
    type: Boolean
  },
  style: {
    type: String
  },
<<<<<<< HEAD

  // References users from Jira and Gitlab Api
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
=======
 
 /* user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  // Get Sprint from Jira APi
  sprints: {
    type: Schema.Types.ObjectId,
    ref: "sprints"
  },
  // Get Commits from Gitlab APi
  Commits: {
    type: Schema.Types.ObjectId,
    ref: "commits"
  },*/
>>>>>>> cefbbd6f70685e8b08b7f6efd9e280633fc76ec0

  

  // ref to Board 
  Board: {
    type: Schema.Types.ObjectId,
    ref: "boards"
<<<<<<< HEAD
  }
=======
  },
  Branch: {
    type: Schema.Types.ObjectId,
    ref: "Branch"
  }



>>>>>>> cefbbd6f70685e8b08b7f6efd9e280633fc76ec0
});

module.exports = Projet = mongoose.model("projets", ProjetSchema);
