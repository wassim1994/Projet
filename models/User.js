const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const socialSchema = new Schema({
  skype: {
    type: String
  },
  linkedin: {
    type: String
  },
  twitter: {
    type: String
  },
  website_url: {
    type: String
  }
});

const applicationRolesSchema = new Schema({
  size: {
    type: Number
  },
  items: {
    type: String
  }
});
const groupsSchema = new Schema({
  size: {
    type: Number
  },
  items: {
    type: String
  }
});
// Create Schema
const UserSchema = new Schema({
  //Login
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  },

  //Donnees API Jira :Get User
  self_Jira: {
    type: String
  },
  key_Jira: {
    type: String
  },
  accountId_Jira: {
    type: String
  },
  name_Jira: {
    type: String
  },
  emailAddress_Jira: {
    type: String
  },
  avatarUrls_Jira: {
    type: String
  },
  displayName_Jira: {
    type: String
  },
  timeZone_Jira: {
    type: String
  },
  groups: [
    {
      groupsSchema
    }
  ],
  applicationRoles: [
    {
      applicationRolesSchema
    }
  ],

  //Donnees API Gitlab :Get User

  id_Gitlab: {
    type: String
  },
  username_Gitlab: {
    type: String
  },

  name_Gitlab: {
    type: String
  },
  state_Gitlab: {
    type: String
  },
  avatar_url_Gitlab: {
    type: String
  },
  web_url_Gitlab: {
    type: String
  },
  created_at_Gitlab: {
    type: Date
  },
  bio_Gitlab: {
    type: String
  },
  location_Gitlab: {
    type: String
  },
  public_email_Gitlab: {
    type: String
  },
  social: {
    socialSchema
  },
  organization_Gitlab: {
    type: String
  }
});

module.exports = User = mongoose.model("users", UserSchema);
