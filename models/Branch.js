const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const BranchSchema = new Schema({
  name: {
    type: String
  },
  merged: {
    type: Boolean
  },
  protected: {
    type: Boolean
  },
  default: {
    type: Boolean
  },
  developers_can_push: {
    type: Boolean
  },
  developers_can_merge: {
    type: Boolean
  },
  can_push: {
    type: Boolean
  },
  projet: {
    type: Schema.Types.ObjectId,
    ref: "projets"
  }
});
module.exports = Branch = mongoose.model("Branchs", BranchSchema);
