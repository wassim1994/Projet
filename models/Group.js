const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
//Gets a list of group or project members viewable by the authenticated user. 
const GroupSchema = new Schema({

  id:{
    Type:Number
  },
  name:{
    Type:String
  },
  path:{
    Type:String
  },
  description:{
    Type:String
  },
  visibility:{
    Type:String
  },
  lfs_enabled:{
    Type:Boolean
  },
  avatar_url:{
    Type:String
  },
  web_url:{
    Type:String
  },
  request_access_enabled:{
    Type:Boolean
  },
  full_name: {
    Type:String
  },
  full_path:{
    Type:String
  },
  file_template_project_id:{
    Type:Number
  },
  parent_id:{
    Type:Number
  },
    projet: {
       type : Schema.Types.ObjectId,
       ref  : "projets"
    }



});

module.exports = Group = mongoose.model('groups', GroupSchema);
