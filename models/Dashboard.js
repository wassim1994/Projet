const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const sharePermissionsSchema = new Schema({
  type: {
    type: String
  }
});
const DashboardSchema = new Schema({
  id: {
    type: String
  },
  isFavourite: {
    type: Boolean
  },
  name: {
    type: String
  },
  popularity: {
    type: Number
  },
  self: {
    type: String
  },
  sharePermissions: [
    {
      sharePermissionsSchema
    }
  ],
  view: {
    type: String
  }
});
module.exports = Dashboard = mongoose.model("dashboards", DashboardSchema);
