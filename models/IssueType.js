const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const IssueTypeSchema = new Schema(
  {

    self:{
        type:String
    }, 
    id:{
        type:Number
    }, 
    description: {
        type:String
    },
    iconUrl:{
        type:String
    },
    name: {
        type:String
    }, 
    subtask: {
        type:Boolean
    } , 
    avatarId: {
        type:Number
    } ,
    issue: {
        type: Schema.Types.ObjectId,
        ref: "issues"
      },
  });

  module.exports = IssueType = mongoose.model("issuetype", IssueTypeSchema);